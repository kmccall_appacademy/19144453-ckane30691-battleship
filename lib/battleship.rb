require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)
    @player, @board = player, board
    @hit = false
  end

  def attack(pos)
    board.grid[pos[0]][pos[1]] = :x 
    board.grid[pos[0]][pos[1]] == :s ? @hit = true : @hit = false
  end

  def hit?
    @hit
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    move = player.get_play
    attack(move)
    display_status
  end

  def display_status
    board.display
    puts "HIT!" if hit?
    puts "There are #{count} ships left."
  end

  def play
    10.times {board.place_random_ship}
    display_status
    play_turn until game_over?
    puts "You win!"
  end

end


if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end