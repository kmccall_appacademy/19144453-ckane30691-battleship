class HumanPlayer
  attr_reader :name

  def initialize(name="Player 1")
    @name = name
  end

  def get_play
    puts "Make a move (row, col)"
    gets.chomp.split(",").map {|num| num.to_i}
  end
end
