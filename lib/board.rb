class Board
  attr_accessor :grid

  DISPLAY_HASH = {
    nil => " ",
    :s => " ",
    :x => "x"
  }

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    result = 0
    grid.each {|row| result += row.count(:s)}
    result
  end

  def empty?(pos = nil)
    if pos == nil
      self.count == 0
    else
      grid[pos[0]][pos[1]] == nil
    end
  end

  def display
    header = (0..9).to_a.join("  ")
    p "  #{header}"
    grid.each_with_index {|row, idx| display_row(row, idx)}
  end

  def display_row(row, idx)
    chars = row.map {|item| DISPLAY_HASH[item]}.join("  ")
    p "#{idx} #{chars}"
  end

  def full?
    grid.select {|row| row.include?(nil)}.empty?
  end

  def won?
    self.empty?
  end

  def [](pos)
    grid[pos[0]][pos[1]]
  end

  def []=(pos, value)
    grid[pos[0]][pos[1]] = value
  end

  def place_random_ship
    raise "full" if full?
    pos = [rand(grid.length), rand(grid.length)]
    until self.empty?(pos)
      pos = [rand(grid.length), rand(grid.length)]
    end
    self[pos] = :s
  end

end
